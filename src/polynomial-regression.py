#!/usr/bin/python3

import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt

# Choose the degree of the polynomial you want to fit
# Set the degree to -1 to see an analyzis for various values
degree = -1

# Set the Tichonov regularization
tichonov = 0

# Choose solver
#solver = "gd"
solver = "scikit"


def ScikitSolver( degree, x, y ):

   # Reshape x to a column vector if it's 1D
   if x.ndim == 1:
      x = x.reshape(-1, 1)

   # Create polynomial features
   poly_features = PolynomialFeatures(degree=degree)
   poly_x = poly_features.fit_transform(x)

   # Fit a linear regression model
   #model = LinearRegression()
   model = Ridge(alpha = tichonov)

   model.fit( poly_x, y)

   # Reverse the order of elements
   coefficients = np.flip( model.coef_ )
   return coefficients


def GradientDescent( degree, x, y, learning_rate, stop_epsilon, max_iterations ):
   # Perform gradient descent
   coefficients = np.zeros(degree + 1)
   last_residue = -5;
   for iteration in range(max_iterations):
      y_pred = np.polyval(coefficients, x )
      gradient = np.zeros( degree+1 )
      for j in range(degree+1):
         for i in range(x.size):
               gradient[ j ] += ( y_pred[i]-y[i] )* x[i]**(degree-j) - tichonov * coefficients[ j ]


      # Update coefficients using gradient descent
      coefficients -= learning_rate * gradient / np.linalg.norm( gradient )
      residue = np.linalg.norm( y_pred - y, 2 )
      if iteration % 100 == 0:
         print( f"iter = {iteration} residue = {residue}" )
      if abs( residue - last_residue ) < stop_epsilon:
         return coefficients
      last_residue = residue
   return coefficients

# Print a polynom to a string
def PolynomToStr( coefficients ):
   poly_str = ""
   for i, coef in enumerate(coefficients[0:], start=0):
      if coef >= 0:
         poly_str += f" + {coef:.2f}"
      else:
         poly_str += f" - {abs(coef):.2f}"
      if i < degree:
         poly_str += f"x^{degree-i}"
   return poly_str

# Plot the original data and the polynomial regression curve
def DrawPolynom( degree, x, y, y_exact, x_fine, coefficients ):
   y_pred = np.polyval(coefficients, x_fine )
   plt.scatter(x, y_exact, label="Original Data", color='green')
   plt.scatter(x, y, label="Data with noise", color='blue')
   plt.plot(x_fine, y_pred, label="Polynomial Regression", color='red')
   plt.xlabel("X")
   plt.ylabel("Y")
   plt.legend()
   plt.title(f"Polynomial Regression (Degree {degree})")
   plt.show()

# Load training and testing data to files
x_train = np.loadtxt('polynomial_regression_x_train.txt' )
y_train = np.loadtxt('polynomial_regression_y_train.txt' )
x_test  = np.loadtxt('polynomial_regression_x_test.txt' )
y_test  = np.loadtxt('polynomial_regression_y_test.txt' )
x       = np.loadtxt('polynomial_regression_x.txt' )
y_exact = np.loadtxt('polynomial_regression_y_exact.txt' )
y       = np.loadtxt('polynomial_regression_y.txt' )
set_size = x.size
x_min = min( x )
x_max = max( x )


if degree >= 0:
   if solver == "scikit":
      coefficients = ScikitSolver( degree, x_train, y_train )
   if solver == "gd":
      coefficients = GradientDescent( degree, x_train, y_train, learning_rate = 0.001, stop_epsilon=0.00001, max_iterations = 1000000 )
   print( PolynomToStr( coefficients ) )

   # Predict the y values
   x_fine = np.linspace(x_min,x_max,10*set_size)
   y_pred = np.polyval(coefficients, x_fine )
   y_pred_train = np.polyval(coefficients, x_train )
   y_pred_test = np.polyval(coefficients, x_test )

   # Calculate the L2 norm (MSE) between predicted and actual values
   mse_train = np.mean((y_train - y_pred_train) ** 2)
   mse_test = np.mean((y_test - y_pred_test) ** 2)
   print(f"Mean Squared Error (MSE) on training data: {mse_train}")
   print(f"Mean Squared Error (MSE) on testing data: {mse_test}")

   DrawPolynom( degree, x, y, y_exact, x_fine, coefficients )
else:
   degrees = []
   train_errors = []
   test_errors = []
   for dgree in range(0,15):
      if solver == "scikit":
         coefficients = ScikitSolver( dgree, x_train, y_train )
      if solver == "gd":
         coefficients = GradientDescent( dgree, x_train, y_train, learning_rate = 0.001, stop_epsilon=0.00001, max_iterations = 1000000 )
      # Predict the y values
      y_pred_train = np.polyval(coefficients, x_train )
      y_pred_test = np.polyval(coefficients, x_test )

      # Calculate the L2 norm (MSE) between predicted and actual values
      mse_train = np.mean((y_train - y_pred_train) ** 2)
      mse_test = np.mean((y_test - y_pred_test) ** 2)
      print(f"Degree {dgree}")
      print(f"  Mean Squared Error (MSE) on training data: {mse_train}")
      print(f"  Mean Squared Error (MSE) on testing data: {mse_test}")
      degrees.append( dgree )
      train_errors.append( mse_train )
      test_errors.append( mse_test )

      # Plot the polynomial curve
      x_fine = np.linspace(x_min,x_max,10*set_size)
      DrawPolynom( dgree, x, y, y_exact, x_fine, coefficients )

   # Plot the analysis of errors for various polynomial degrees
   plt.scatter(degrees, train_errors, label="Training error", color='green')
   plt.scatter(degrees, test_errors, label="Testing error", color='red')
   plt.xlabel("Degree")
   plt.ylabel("Error")
   plt.yscale("log")
   plt.legend()
   plt.title(f"Polynomial Regression Error Analysis")
   plt.show()


