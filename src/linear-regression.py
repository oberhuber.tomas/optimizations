#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

# Set data set name
#filename='linear_regression_data.txt'
#filename='anscombe_1.txt'
#filename='anscombe_2.txt'
#filename='anscombe_3.txt'
#filename='anscombe_4.txt'

# Step 1: Read the data from the text file
data = np.loadtxt(filename, delimiter=',')


def draw( x, y, w, b ):
   plt.scatter(x, y, label="Original Data", color='blue')
   plt.plot(x, w * x + b, label="Linear Regression", color='red')
   plt.xlabel("X")
   plt.ylabel("Y")
   plt.legend()
   plt.title("Linear Regression with Gradient Descent")
   plt.show()

# Extract independent variable (x) and dependent variable (y)
x = data[:, 0]
y = data[:, 1]

# Hyperparameters for gradient descent
learning_rate = 0.01
iterations = 20

# Initialize coefficients (slope and bias)
w = 0 # slope
b = 0 # bias

# Step 2: Implement gradient descent to fit the linear regression model
n = len(x)
for i in range(iterations):
    y_pred = w * x + b
    w_gradient = 2/n * np.sum((y_pred-y)*x)
    b_gradient = 2/n * np.sum(y_pred-y)
    w -= learning_rate * w_gradient
    b -= learning_rate * b_gradient
    if  i % 2 == 0:
      diff = y_pred-y
      mse = 1/n * np.sum(diff ** 2 )
      print( f'Iteration {i}: w = {w} b = {b} mse = {mse}'  )
      draw( x, y, w, b )

# Step 3: Plot the original data and the linear regression line
draw( x, y, w, b)

# Print the coefficients
print(f"Slope (w)): {w}")
print(f"Bias (b): {b}")
