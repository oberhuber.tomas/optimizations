#!/usr/bin/python3
import numpy as np

# Generate synthetic data
np.random.seed(0)  # For reproducibility

# Generate random coefficients (w) and intercept (b)
w = np.array([2.0])  # Adjust the values as needed
b = 1.0  # Adjust the intercept as needed

# Generate the independent variable (x)
x = np.linspace(0, 10, 100)

# Generate random noise following a normal distribution
noise = np.random.normal(0, 1, 100)

# Create the dependent variable (y) as a linear combination of x, w, and noise
y = w * x + b + noise

# Combine x and y into a 2D array
data = np.column_stack((x, y))

# Save the data to a text file
np.savetxt('linear_regression_data.txt', data, fmt='%0.6f', delimiter=', ')
