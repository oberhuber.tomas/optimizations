#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron

# Load your dataset (replace 'dataset.csv' with your dataset file)
# You need to specify the delimiter or separator used in your dataset (e.g., ',' for CSV)
# Replace 'your_dataset.csv' with the actual path to your dataset file.
# You can use np.loadtxt or np.genfromtxt depending on your dataset format.
# Example for CSV:
dataset = np.genfromtxt('binary-classification-data.txt', delimiter=',')

# Choose two classes for binary classification
class_1 = 0  # Replace with the actual class label
class_2 = 1  # Replace with the actual class label

tests  = [ 'least-squares', 'perceptron', 'svm', 'logistic-regression']

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def report( X_test, y_test, theta ):
   # Make predictions on the testing data
   y_pred = (X_test.dot(theta) >= 0.5).astype(int)

   # Evaluate the classifier's performance
   accuracy = accuracy_score(y_test, y_pred)
   #classification_rep = classification_report(y_test, y_pred)

   # Print the accuracy and classification report
   print(f"Accuracy: {accuracy:.2f}")
   #print("Classification Report:\n", classification_rep)

def plot_classification_result(X_train, y_train, X_test, y_test, theta, title):
    plt.figure(figsize=(12, 6))

    # Plot the training data and decision boundary
    plt.subplot(1, 2, 1)
    plt.scatter(X_train[y_train == 0][:, 1], X_train[y_train == 0][:, 2], marker='o', label='Class 0 (Train)', color='blue')
    plt.scatter(X_train[y_train == 1][:, 1], X_train[y_train == 1][:, 2], marker='x', label='Class 1 (Train)', color='red')

    # Create the decision boundary line using theta
    x_values = np.array([np.min(X_train[:, 1]) - 1, np.max(X_train[:, 1]) + 1])
    y_values = (-theta[0] - (theta[1] * x_values)) / theta[2]

    # Plot the decision boundary line for training data
    plt.plot(x_values, y_values, label='Decision Boundary (Train)', linestyle='--', color='green')

    plt.xlabel('Feature 1')
    plt.ylabel('Feature 2')
    plt.legend()
    plt.title(f'{title} (Training Data)')

    # Plot the test data and decision boundary
    plt.subplot(1, 2, 2)
    plt.scatter(X_test[y_test == 0][:, 1], X_test[y_test == 0][:, 2], marker='o', label='Class 0 (Test)', color='blue')
    plt.scatter(X_test[y_test == 1][:, 1], X_test[y_test == 1][:, 2], marker='x', label='Class 1 (Test)', color='red')

    # Plot the decision boundary line for test data
    plt.plot(x_values, y_values, label='Decision Boundary (Train)', linestyle='--', color='green')

    plt.xlabel('Feature 1')
    plt.ylabel('Feature 2')
    plt.legend()
    plt.title(f'{title} (Test Data)')

    plt.tight_layout()
    plt.show()


def least_squares( X_train, y_train, gamma, num_iterations, learning_rate ):
   # Initialize model parameters (weights)
   theta = np.zeros(X_train.shape[1])

   # Gradient Descent to optimize least squares loss
   for i in range(num_iterations):
      # Calculate predictions using the current weights
      predictions = X_train.dot(theta)

      # Calculate the gradient of the loss function
      gradient = (X_train.T.dot(predictions - y_train)) / len(y_train) - gamma * theta

      # Update the weights using gradient descent
      theta -= learning_rate * gradient
   return theta

def scikit_svm(X_train, y_train):
    # Create and train an SVM classifier with a linear kernel
    svm_classifier = SVC(kernel="linear")
    svm_classifier.fit(X_train[:, 1:], y_train)
    return np.concatenate(( svm_classifier.intercept_, svm_classifier.coef_[0] ))

def svm( X_train, y_train, gamma, num_iterations, learning_rate, type ):
   if type != "L2" and type != "hinge":
       raise Exception( f"Unknown SVM type {type}" )

   # Initialize model parameters (weights)
   num_samples, num_parameters  = X_train.shape
   theta = np.zeros( num_parameters )

   # Transform y_train from {0,1} to {-1,1}
   y_train = 2*( y_train - 0.5)

   # Gradient descent optimization
   for iter in range(num_iterations):
      # Calculate predictions using the current weights
      predictions = X_train.dot(theta)

      # Compute the gradient
      gradient = np.zeros( num_parameters )
      loss = 1 - y_train * predictions
      if type == "L2":
         for i in range(num_samples):
               if loss[ i ] > 0:
                  gradient += - loss[i]*y_train[i]*X_train[i] + gamma * theta

      if type == "hinge":
         for i in range(num_samples):
               if loss[ i ] > 0:
                  gradient += - y_train[i]*X_train[i] + gamma * theta

      # Update the weights and bias using the gradient
      theta -= learning_rate * gradient
   return theta

def scikit_logistic_regression(X_train, y_train, num_iterations):
    # Create and train a logistic regression classifier
    logreg_classifier = LogisticRegression(max_iter=num_iterations)
    logreg_classifier.fit(X_train[:, 1:], y_train)
    return np.concatenate(( logreg_classifier.intercept_, logreg_classifier.coef_[0] ))

def logistic_regression(X_train, y_train, gamma, num_iterations, learning_rate):
   # Initialize model parameters (weights)
   num_samples, num_parameters  = X_train.shape
   theta = np.zeros( num_parameters )

   for i in range(num_iterations):
        z = np.dot(X_train, theta)
        h = sigmoid(z)
        gradient = np.dot(X_train.T, (h - y_train)) / num_samples + gamma * theta
        theta -= learning_rate * gradient

   return theta

def scikit_perceptron(X_train, y_train, num_iterations, learning_rate):
    perceptron_classifier = Perceptron(max_iter=num_iterations)
    perceptron_classifier.fit(X_train[:,1:], y_train)
    return np.concatenate(( perceptron_classifier.intercept_, perceptron_classifier.coef_[0] ))

def perceptron( X_train, y_train, num_iterations, learning_rate ):
   # Initialize model parameters (weights)
   num_samples, num_parameters  = X_train.shape
   theta = np.zeros( num_parameters )

   # Transform y_train from {0,1} to {-1,1}
   y_train = 2*( y_train - 0.5)

   # Gradient descent optimization
   for iter in range(num_iterations):
      # Calculate predictions using the current weights
      predictions = X_train.dot(theta)

      # Compute the gradient
      gradient = np.zeros( num_parameters )
      aux = - y_train * predictions
      for i in range(num_samples):
            if aux[i] > 0:
               gradient += - y_train[i]*X_train[i]

      # Update the weights and bias using the gradient
      theta -= learning_rate * gradient
   return theta


# Create a binary classification dataset with only the chosen classes
# dataset[:, -1] means "take all rows and the last column"
binary_dataset = dataset[(dataset[:, -1] == class_1) | (dataset[:, -1] == class_2)]

# Split the dataset into features (X) and the target variable (y)
X = binary_dataset[:, :-1]  # Features as a matrix
y = (binary_dataset[:, -1] == class_1).astype(int)  # Binary labels (0 or 1)

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Add a bias term (intercept) to the feature matrix - it will be the first column
X_train = np.hstack((np.ones((X_train.shape[0], 1)), X_train))
X_test = np.hstack((np.ones((X_test.shape[0], 1)), X_test))

# Perform classification using the least squares method
if 'least-squares' in tests:
   lst_learning_rate = 0.01
   lst_num_iterations = 10000
   lst_gamma = 1   # regularization
   print( "Classification with least squares:" )
   lst_theta = least_squares( X_train, y_train, lst_gamma, lst_num_iterations, lst_learning_rate )
   report( X_test, y_test, lst_theta )
   print( f'theta = {lst_theta}')
   plot_classification_result(X_train, y_train, X_test, y_test, lst_theta, 'Least Squares Classification')

# Perform classification using the preceptron
if 'perceptron' in tests:
   per_learning_rate = 0.01
   per_num_iterations = 100000
   print( "Classification with perceptron:" )
   per_theta = scikit_perceptron( X_train, y_train, per_num_iterations, per_learning_rate )
   report( X_test, y_test, per_theta )
   plot_classification_result(X_train, y_train, X_test, y_test, per_theta, 'Perceptron Classification')

# Perform classification using the SVM
if 'svm' in tests:
   svm_learning_rate = 0.01
   svm_num_iterations = 10000
   svm_gamma = 1   # regularization
   print( "Classification with SVM:" )
   #svm_theta = svm( X_train, y_train, svm_gamma, svm_num_iterations, svm_learning_rate, "hinge" )
   svm_theta = scikit_svm( X_train, y_train )
   print( f'theta = {svm_theta}' )
   report( X_test, y_test, svm_theta )
   plot_classification_result(X_train, y_train, X_test, y_test, svm_theta, 'SVM Classification')

# Perform classification using the logistic regression
if 'logistic-regression' in tests:
   log_learning_rate = 0.01
   log_num_iterations = 1000
   log_gamma = 1   # regularization
   print( "Classification with Logistic regression:" )
   #log_theta = logistic_regression( X_train, y_train, log_gamma, log_num_iterations, log_learning_rate )
   log_theta = scikit_logistic_regression( X_train, y_train, log_num_iterations )
   print( f'theta = {log_theta}' )
   report( X_test, y_test, log_theta )
   plot_classification_result(X_train, y_train, X_test, y_test, log_theta, 'Logistic regression Classification')

