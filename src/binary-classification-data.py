#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

# Set random seed for reproducibility
np.random.seed(0)

# Number of data points in each class
num_samples = 100

# Control the overlap with a parameter (increase for more overlap)
overlap_parameter = 0

# Generate two classes of data with different means and adjustable covariances
mean_class1 = [2, 2]
cov_class1 = [[1, overlap_parameter], [overlap_parameter, 1]]
class1_data = np.random.multivariate_normal(mean_class1, cov_class1, num_samples)

mean_class2 = [6, 6]
cov_class2 = [[1, -overlap_parameter], [-overlap_parameter, 1]]
class2_data = np.random.multivariate_normal(mean_class2, cov_class2, num_samples)

# Combine the two classes into a single dataset
X = np.vstack((class1_data, class2_data))
y = np.hstack((np.zeros(num_samples), np.ones(num_samples)))

# Save the generated data to a file with labels as the last column
data_with_labels = np.column_stack((X, y))
np.savetxt('binary-classification-data.txt', data_with_labels, delimiter=',' )

# Plot the generated data
plt.figure(figsize=(8, 6))
plt.scatter(class1_data[:, 0], class1_data[:, 1], marker='o', label='Class 0')
plt.scatter(class2_data[:, 0], class2_data[:, 1], marker='x', label='Class 1')
plt.xlabel('Feature 1')
plt.ylabel('Feature 2')
plt.legend()
plt.title('Generated Data for Binary Linear Classification')
plt.show()
