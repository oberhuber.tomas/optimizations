#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Set the random seed for reproducibility
np.random.seed(1113)

# Number of data points per class
num_samples_per_class = 50

# Number of classes
num_classes = 4

# Number of features (2 for 2D data)
num_features = 2

# Generate random data for each class
data = []
for class_label in range(num_classes):
    # Randomly generate mean for each class
    mean = np.random.rand(num_features) * 10

    # Generate a positive-semidefinite covariance matrix
    while True:
        covariance_matrix = np.random.rand(num_features, num_features)
        covariance_matrix = np.dot(covariance_matrix, covariance_matrix.T)
        if np.all(np.linalg.eigvals(covariance_matrix) > 0):
            break

    # Generate data points for the current class
    class_data = np.random.multivariate_normal(mean, covariance_matrix, num_samples_per_class)

    # Create labels for the class
    class_labels = np.full((num_samples_per_class, 1), class_label)

    # Combine data points and labels for the current class
    class_data = np.hstack((class_data, class_labels))

    data.append(class_data)

# Concatenate data from all classes
data = np.vstack(data)

# Create a DataFrame for the data
column_names = [f'feature_{i}' for i in range(num_features)] + ['class']
df = pd.DataFrame(data, columns=column_names)

# Save the data to a CSV file
#df.to_csv('multiclass-data.csv', index=False)
np.savetxt('multiclass-data.csv', data, delimiter=',' )

# Plot the data points
colors = ['r', 'g', 'b']  # Colors for each class
plt.figure(figsize=(8, 6))
for class_label in range(num_classes):
    class_data = df[df['class'] == class_label]
    #plt.scatter(class_data['feature_0'], class_data['feature_1'], c=colors[class_label], label=f'Class {class_label}')
    plt.scatter(class_data['feature_0'], class_data['feature_1'], label=f'Class {class_label}')

plt.xlabel('Feature 0')
plt.ylabel('Feature 1')
plt.legend()
plt.title('Synthetic Multiclass Classification Data')
plt.show()
