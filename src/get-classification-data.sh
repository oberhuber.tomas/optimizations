#!/bin/bash

wget https://archive.ics.uci.edu/static/public/94/spambase.zip
unzip spambase.zip

wget https://archive.ics.uci.edu/static/public/53/iris.zip
unzip iris.zip

wget https://archive.ics.uci.edu/static/public/73/mushroom.zip
unzip mushroom.zip