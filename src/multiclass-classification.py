#!/usr/bin/python3

import numpy as np
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
import matplotlib.pyplot as plt

# Load your dataset from a file (replace 'dataset.csv' with your file)
# You can use np.loadtxt or np.genfromtxt depending on your file format.
# Example for CSV:
# dataset = np.loadtxt('dataset.csv', delimiter=',')
dataset = np.genfromtxt('multiclass-data.csv', delimiter=',')

# Split the dataset into features (X) and the target variable (y)
X = dataset[:, :-1]  # Features as a matrix
y = dataset[:, -1]  # Class labels

# Get the number of unique classes
num_classes = len(np.unique(y))

# Convert target labels to one-hot encoded format
def one_hot_encode(y, num_classes):
    one_hot = np.zeros((len(y), num_classes))
    one_hot[np.arange(len(y)), y.astype(int)] = 1
    return one_hot

# Softmax function
def softmax(z):
    exp_z = np.exp(z - np.max(z, axis=1, keepdims=True))  # Subtract max for numerical stability
    return exp_z / np.sum(exp_z, axis=1, keepdims=True)

def report( X, theta ):
   # Classify data points using the trained models
   probabilities = X.dot(theta.T)
   classifications = np.argmax(probabilities, axis=1)

   #print( f'classifications={classifications}' )
   # Calculate accuracy and print classification report
   accuracy = accuracy_score(y, classifications)
   #classification_rep = classification_report(y, classifications)

   print(f"Accuracy: {accuracy:.2f}")
   #print("Classification Report:\n", classification_rep)

def report_classifier( X, classifier ):
   # Classify data points using the trained models
   classifications = classifier.predict(X[:, 1:])

   #print( f'classifications={classifications}' )
   # Calculate accuracy and print classification report
   accuracy = accuracy_score(y, classifications)
   #classification_rep = classification_report(y, classifications)

   print(f"Accuracy: {accuracy:.2f}")
   #print("Classification Report:\n", classification_rep)


def draw( X, theta, title ):
   # Create a subplot for each pair of classes
   plt.figure(figsize=(12, 8))

   # Define the range for the meshgrid
   x_min, x_max = X[:, 1].min() - 1, X[:, 1].max() + 1
   y_min, y_max = X[:, 2].min() - 1, X[:, 2].max() + 1

   # Create a meshgrid
   xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.025),
                        np.arange(y_min, y_max, 0.025))

   # Combine xx and yy into a new array of the desired form
   xx_yy = np.column_stack((xx.flatten(), yy.flatten()))
   xx_yy = np.hstack((np.ones((xx_yy.shape[0], 1)), xx_yy))

   z = np.argmax(xx_yy.dot(theta.T),1)

   # Reshape the z array to match the meshgrid dimensions

   z = z.reshape(xx.shape)

   # Plot data points
   for class_label in range(num_classes):
      class_data = dataset[dataset[:, -1] == class_label]
      plt.scatter(class_data[:, 0], class_data[:, 1], label=f'Class {class_label}', cmap=plt.cm.Spectral)

   # Plot the decision boundary as a contour plot within the meshgrid range
   plt.contourf(xx, yy, z, cmap=plt.cm.Spectral, alpha=0.8, levels=[0.5,1.5,2.5,3.5,4.5,5.5,6.5])
   #plt.contourf(xx, yy, z, alpha=0.8, levels=[0.5,1.5,2.5])

   plt.xlabel('Feature 1')
   plt.ylabel('Feature 2')
   plt.title(f' Decision Boundary for ' + title)

   plt.tight_layout()
   plt.show()


def draw_classifier( X, classifier, title ):
   # Create a subplot for each pair of classes
   plt.figure(figsize=(12, 8))

   # Define the range for the meshgrid
   x_min, x_max = X[:, 1].min() - 1, X[:, 1].max() + 1
   y_min, y_max = X[:, 2].min() - 1, X[:, 2].max() + 1

   # Create a meshgrid
   xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.025),
                        np.arange(y_min, y_max, 0.025))

   # Combine xx and yy into a new array of the desired form
   xx_yy = np.column_stack((xx.flatten(), yy.flatten()))
   #xx_yy = np.hstack((np.ones((xx_yy.shape[0], 1)), xx_yy))

   #z = np.argmax(xx_yy.dot(theta.T),1)
   z = classifier.predict(xx_yy)

   # Reshape the z array to match the meshgrid dimensions

   z = z.reshape(xx.shape)

   print( f'z=z{z}' )

   # Plot data points
   for class_label in range(num_classes):
      class_data = dataset[dataset[:, -1] == class_label]
      plt.scatter(class_data[:, 0], class_data[:, 1], label=f'Class {class_label}')

   # Plot the decision boundary as a contour plot within the meshgrid range
   plt.contourf(xx, yy, z, cmap=plt.cm.Spectral, alpha=0.8, levels=[0.5,1.5,2.5,3.5,4.5,5.5,6.5])

   plt.xlabel('Feature 1')
   plt.ylabel('Feature 2')
   plt.title(f' Decision Boundary for ' + title)

   plt.tight_layout()
   plt.show()


def least_squares( X_train, y_train, gamma, num_iterations, learning_rate ):
   # Initialize model parameters (weights) for each class, including bias
   y_train_oh = one_hot_encode(y_train, num_classes)
   num_parameters = X.shape[1]
   theta = np.zeros((num_classes, num_parameters))

   for i in range(num_iterations):
      predictions = X_train.dot(theta.T)
      gradient = (X_train.T.dot(predictions - y_train_oh)) / len(y_train_oh) - gamma * theta.T
      theta -= learning_rate * gradient.T
   return theta

def scikit_svm(X_train, y_train, gamma):
   svm_classifier = SVC(kernel="linear", decision_function_shape='ovr', C=1.0/gamma)
   svm_classifier.fit(X_train[:, 1:], y_train)
   #svm_classifier.fit(X_train, y_train)
   #print( f'svm_classifier.intercept_ = {svm_classifier.intercept_}')
   #print( f'svm_classifier.coef_  = {svm_classifier.coef_ }')
   #theta = np.hstack( ( svm_classifier.intercept_.reshape(-1,1), svm_classifier.coef_ ) )
   # TODO: Extract parameters of the classifier

   return svm_classifier

def svm( X_train, y_train, gamma, num_iterations, learning_rate, margin=1 ): # set margin = 0 for perceptron
   # Initialize model parameters (weights) for each class, including bias
   # y_train_oh = one_hot_encode(y_train, num_classes)
   num_parameters = X.shape[1]
   w = np.zeros((num_parameters,num_classes))

   for i in range(num_iterations):
      v = X_train.dot( w ) + margin
      gradient = np.zeros((num_classes, num_parameters))
      for i in range( X_train.shape[0] ):
         for r in range( num_classes ):
            if r == y_train[ i ]:
               for j in range( num_classes ):
                  if j != r and v[ i ][ j ] > 0:
                     gradient[ r ] -= X_train[ i ]
            elif v[i][ r ] > 0:
               gradient[ r ] = +X_train[ i ]
      w -= learning_rate * gradient.T
   return w.T

def scikit_logistic_regression(X_train, y_train, gamma):
   logistic_classifier = LogisticRegression(solver='lbfgs', max_iter=1000, multi_class='multinomial', C=1.0/gamma)
   logistic_classifier.fit(X_train[:,1:], y_train)
   # TODO: Extract parameters of the classifier
   return logistic_classifier

def logistic_regression( X_train, y_train, gamma, num_iterations, learning_rate ):
   y_one_hot = one_hot_encode(y_train, num_classes)
   num_samples, num_parameters = X.shape
   weights = np.zeros((num_parameters, num_classes))
   for _ in range(num_iterations):
      # Calculate predictions using the current weights and biases
      scores = np.dot(X_train, weights)
      probabilities = softmax(scores)

      # Calculate the gradient of the softmax loss
      gradient = np.dot(X_train.T, (probabilities - y_one_hot)) / num_samples + gamma * weights

      # Update the weights and biases using gradient descent
      weights -= learning_rate * gradient

   return weights.T

def scikit_perceptron(X_train, y_train ):
   perceptron_classifier = Perceptron(max_iter=1000)
   perceptron_classifier.fit(X_train[:,1:], y_train)
   # TODO: Extract parameters of the classifier
   return perceptron_classifier

def precpetron():
   y_one_hot = one_hot_encode(y_train, num_classes)
   num_samples, num_parameters = X.shape
   weights = np.zeros((num_parameters, num_classes))
   for _ in range(num_iterations):
      for i in range(num_classes):
         # Compute scores for class i
         scores = X_train.dot(weights[i])

         # Compute predictions using the softmax function
         exp_scores = np.exp(scores - np.max(scores))
         softmax_probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)

         # Compute gradients
         gradient = -(1 / num_samples) * X_train.T.dot(y_one_hot[:, i] - softmax_probs[:, i])

         # Update weights and biases
         weights[i] -= learning_rate * gradient
         biases[i] -= learning_rate * np.sum(y_one_hot[:, i] - softmax_probs[:, i])



# Add a column of ones for the bias term in the feature matrix
X = np.hstack((np.ones((X.shape[0], 1)), X))

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Perform classification using the least squares method
print( "Least squares Classification")
lst_learning_rate = 0.01
lst_num_iterations = 10000
lst_gamma = 1   # regularization
lst_weights = least_squares( X_train, y_train, lst_gamma, lst_num_iterations, lst_learning_rate )
print( f'w={lst_weights}' )
report( X, lst_weights)
#draw( X_train, lst_weights, 'Least squares classification' )

# Perform classification using Scikit SVM
print( "Scikit SVM Classification")
sk_svm_learning_rate = 0.01
sk_svm_num_iterations = 10000
sk_svm_gamma = 0.1   # regularization
sk_svm_classifier = scikit_svm( X_train, y_train, sk_svm_gamma )
#print( f'theta={svm_theta}' )
report_classifier( X, sk_svm_classifier)
#draw_classifier( X_train, sk_svm_classifier,  'Scikit SVM classification' )

# Perform classification using SVM
print( "SVM Classification")
svm_learning_rate = 0.01
svm_num_iterations = 10000
svm_gamma = 0.1   # regularization
svm_weights = svm( X_train, y_train, svm_gamma, svm_num_iterations, svm_learning_rate )
print( f'w={svm_weights}' )
report( X, svm_weights)
#draw( X_train, svm_weights,  'SVM classification' )

# Perform classification using Scikit Logistic regression
print( "Scikit Logistic regression Classification")
sk_log_learning_rate = 0.01
sk_log_num_iterations = 10000
sk_log_gamma = 0.1   # regularization
sk_log_classifier = scikit_logistic_regression( X_train, y_train, sk_log_gamma )
#print( f'theta={svm_theta}' )
report_classifier( X, sk_log_classifier)
#draw_classifier( X_train, sk_log_classifier,  'Scikit Logistic regression classification' )

# Perform classification using logistic regression
print( "Logistic regression Classification")
log_learning_rate = 0.01
log_num_iterations = 10000
log_gamma = 0.1   # regularization
log_weights = logistic_regression( X_train, y_train, svm_gamma, svm_num_iterations, svm_learning_rate )
print( f'w={log_weights}' )
report( X, log_weights)
#draw( X_train, log_weights,  'Logistic regression classification' )

# Perform classification using Scikit percpetron
print( "Scikit Perceptron Classification")
sk_per_learning_rate = 0.01
sk_per_num_iterations = 10000
sk_per_gamma = 0.1   # regularization
sk_per_classifier = scikit_perceptron( X_train, y_train )
#print( f'theta={svm_theta}' )
report_classifier( X, sk_per_classifier)
#draw_classifier( X_train, sk_per_classifier,  'Scikit Perceptron classification' )

# Perform classification using perceptron
print( "Perceptron Classification")
per_learning_rate = 0.01
per_num_iterations = 10000
per_weights = svm( X_train, y_train, 0, per_num_iterations, per_learning_rate, margin=0 )
#print( f'w={per_weights}' )
report( X, per_weights)
draw( X_train, per_weights,  'Perceptron classification' )
