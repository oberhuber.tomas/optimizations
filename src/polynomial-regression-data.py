#!/usr/bin/python3

import numpy as np

# Sample data
set_size = 15
std_dev = 0.2
x = np.linspace(-3,3,set_size)
y_exact = np.sin( x )
e = np.random.normal(0.0, std_dev, set_size)
y = y_exact + e

# Specify the percentage of data to use for training (e.g., 70%)
train_percentage = 0.7

# Calculate the number of data points to use for training
num_train_samples = int(train_percentage * len(x))

# Create an array of indices and shuffle them randomly
indices = np.arange(len(x))
np.random.shuffle(indices)

# Select the first 'num_train_samples' indices for training
train_indices = indices[:num_train_samples]

# Use the selected indices to extract training data
x_train = x[train_indices]
y_train = y[train_indices]

# Use the remaining indices for testing (optional)
test_indices = indices[num_train_samples:]
x_test = x[test_indices]
y_test = y[test_indices]

# Save training and testing data to files
np.savetxt('polynomial_regression_x.txt', x)
np.savetxt('polynomial_regression_y.txt', y)
np.savetxt('polynomial_regression_y_exact.txt', y_exact)
np.savetxt('polynomial_regression_x_train.txt', x_train)
np.savetxt('polynomial_regression_y_train.txt', y_train)
np.savetxt('polynomial_regression_x_test.txt', x_test)
np.savetxt('polynomial_regression_y_test.txt', y_test)


