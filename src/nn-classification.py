#!/usr/bin/python

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras import regularizers
from keras.regularizers import l1_l2
from keras.regularizers import l2
from sklearn.metrics import accuracy_score, classification_report

# Load your dataset
# Replace 'your_dataset.csv' with the actual path to your dataset file.
# You can use pd.read_csv for loading a CSV file.
# Example:
dataset = pd.read_csv('wdbc.data',sep=',')

# Extract features (X) and labels (y)
X = dataset.iloc[:, :-1].values  # Assuming the last column is the label column
y = dataset.iloc[:, -1].values

# Calculate the number of features and unique classes in the dataset
num_classes = len(np.unique(y))
num_features = X.shape[1]

print( f'Number of features: {num_features}')
print( f'Number of class: {num_classes}' )

# Use LabelEncoder to convert string labels to integers
label_encoder = LabelEncoder()
y_int = label_encoder.fit_transform(y)

# Assuming you have already loaded your dataset into X (features) and y (labels).
# Split the dataset into training and testing sets (80% train, 20% test)
X_train, X_test, y_train, y_test = train_test_split(X, y_int, test_size=0.2, random_state=42)

# Standardize the feature data (mean=0, std=1)
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Convert class labels to one-hot encoding (if not already)
y_train_one_hot = to_categorical(y_train)
y_test_one_hot = to_categorical(y_test)

# Create a sequential neural network model
model = Sequential()

# Add input layer and hidden layers
model.add(Dense(units=64, kernel_regularizer=regularizers.L2(0.01), activation='relu', input_dim=num_features ))
model.add(Dense(units=32, kernel_regularizer=regularizers.L2(0.01), activation='relu' ))
model.add(Dense(units=32, kernel_regularizer=regularizers.L2(0.01), activation='relu' ))
model.add(Dense(units=16, kernel_regularizer=regularizers.L2(0.01), activation='relu' ))

# Add output layer with softmax activation for multiclass classification
model.add(Dense(units=num_classes, activation='softmax'))

# Compile the model
model.compile(loss='categorical_crossentropy', optimizer=Adam(learning_rate=0.0001), metrics=['accuracy'])

# Train the model
model.fit(X_train, y_train_one_hot, epochs=100, batch_size=32, validation_split=0.2)

# Make predictions on the testing data
y_pred_one_hot = model.predict(X_test)
y_pred = np.argmax(y_pred_one_hot, axis=1)

# Evaluate the classifier's performance
accuracy = accuracy_score(y_test, y_pred)
classification_rep = classification_report(y_test, y_pred)

# Print the accuracy and classification report
print(f"Accuracy: {accuracy:.2f}")
print("Classification Report:\n", classification_rep)
